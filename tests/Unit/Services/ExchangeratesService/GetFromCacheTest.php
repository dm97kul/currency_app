<?php

namespace Tests\Unit\Services\ExchangeratesService;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use App\Services\ExchangeratesService;
use App\Models\Exchangerate;
use Tests\TestCase;

/**
 * Class GetFromCacheTest
 * @package Tests\Unit\Services\ExchangeratesService
 */
class GetFromCacheTest extends TestCase
{
    use RefreshDatabase;

    /** @var Exchangerate */
    private $exrate;

    /** @var ExchangeratesService */
    private $exratesService;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->exratesService = new ExchangeratesService();

        $this->exrate = Exchangerate::factory()->create();
    }
    /**
     * Тест получения курсов валют из кэша
     *
     * @return void
     */
    public function test_get_from_cache()
    {
        $key = 'exchange_rates_' . Carbon::now()->format('Y-m-d');

        Cache::put($key, $this->exrate);

        $result = $this->exratesService->getFromCache($key);

        $this->assertEquals($result, $this->exrate);

        Cache::forget($key);
    }
}
