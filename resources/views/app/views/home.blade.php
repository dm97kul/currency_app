<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="p-6 sm:px-20 bg-white border-b border-gray-200">
                    <div class="text-lg">
                        Курс валют по отношению к Белорусскому рублю (BYN) на
                        <input onchange="setExratesDate();" type="date" id="exratesDate" value="{{ $data['curr_date'] }}" />
                    </div>
                    <div class="py-2">
                    </div>
                    <div class="bg-opacity-25 grid">
                        @if(count($data['curr_rates']) >0)
                            <table class="table-auto">
                                <thead>
                                <tr>
                                    <th class="border-b text-left">Код</th>
                                    <th class="border-b text-left">Наименование</th>
                                    <th class="border-b text-left">Количество единиц валюты</th>
                                    <th class="border-b text-left">НБ РБ</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($data['curr_rates'] as $rate)
                                        <tr>
                                            <td>{{ $rate['code'] }}</td>
                                            <td>{{ $rate['name'] }}</td>
                                            <td>{{ $rate['scale'] }}</td>
                                            <td>{{ $rate['official_rate'] }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <h4>Курс валют на запрашиваемую дату не установлен</h4>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function setExratesDate() {
            document.location.href = "{{ route('home') }}?on_date=" + document.getElementById('exratesDate').value;
        }
    </script>
</x-app-layout>
