<?php

namespace Tests\Unit\Services\ApiService;

use Illuminate\Support\Carbon;
use App\Services\ApiService;
use Tests\TestCase;

/**
 * Class GetTest
 * @package Tests\Unit\Services\ApiService
 */
class GetTest extends TestCase
{
    /** @var ApiService  */
    protected $apiService;

    /** @var string  */
    protected $baseUrl;

    /** @var string  */
    protected $date;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->baseUrl = config('exchangerates.nbrb_url');

        $this->date = Carbon::now()->format('Y-m-d');

        $this->apiService = new ApiService($this->baseUrl);
    }

    /**
     * Тест запроса на получение данных
     *
     * @return void
     */
    public function test_get()
    {
        $response = $this->apiService->get('rates', [
            'ondate' => $this->date,
            'periodicity' => 0
        ]);

        $this->assertEquals($response->getStatusCode(), 200);
        $this->assertNotEmpty($response->getBody()->getContents());
    }
}
