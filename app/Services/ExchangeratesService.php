<?php

namespace App\Services;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Carbon;
use App\Models\Exchangerate;

/**
 * Class ExchangeRates
 * @package App\Services
 */
class ExchangeratesService
{
    /**
     * @param string $date
     * @return array|mixed|null
     */
    public function getExchangeRatesByDate(string $date)
    {
        $key = 'exchange_rates_' . $date;

        $exrates = $this->getFromCache($key);

        if (is_null($exrates))
        {
            $exrates = $this->getFromDatabase($date);

            if (count($exrates) === 0)
            {
                $exrates = $this->getFromExternalSource($date);

                if (count($exrates) > 0)
                {
                    $this->setToDatabase($exrates);
                    $this->setToCache($key, $exrates);
                }
            } else {
                $this->setToCache($key, $exrates);
            }
        }

        return $exrates;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getFromCache(string $key)
    {
        return Cache::get($key, null);
    }

    /**
     * @param string $date
     * @return mixed
     */
    public function getFromDatabase(string $date)
    {
        return Exchangerate::where('on_date', $date)->get();
    }

    /**
     * @param string $date
     * @return array|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getFromExternalSource(string $date)
    {
        $apiService = new ApiService(config('exchangerates.nbrb_url'));

        $response = $apiService->get('rates', ['ondate' => $date,'periodicity' => 0]);

        if ($response->getStatusCode() === 200)
        {
            $data = json_decode($response->getBody()->getContents(), true);
            $exrates = [];

            foreach ($data as $item) {
                $rateDate = Carbon::parse($item['Date']);
                $exrates[] = [
                    'code' => $item['Cur_Abbreviation'],
                    'name' => $item['Cur_Name'],
                    'scale' => $item['Cur_Scale'],
                    'official_rate' => $item['Cur_OfficialRate'],
                    'on_date' => $rateDate->format('Y-m-d'),
                    'on_time' => $rateDate->format('H:i:s')
                ];
            }

            return $exrates;
        } else {
            return null;
        }
    }

    /**
     * @param string $key
     * @param $data
     */
    public function setToCache(string $key, $data)
    {
        Cache::put($key, $data, 60);
    }

    /**
     * @param array $data
     */
    public function setToDatabase(array $data)
    {
        foreach ($data as $item)
        {
            Exchangerate::create($item);
        }
    }
}
