<?php

namespace Tests\Unit\Services\ExchangeratesService;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;
use App\Services\ExchangeratesService;
use App\Traits\ArrayTestCaseTrait;
use Tests\TestCase;

/**
 * Class GetFromExternalSourceTest
 * @package Tests\Unit\Services\ExchangeratesService
 */
class GetFromExternalSourceTest extends TestCase
{
    use RefreshDatabase, ArrayTestCaseTrait;

    /** @var ExchangeratesService */
    private $exratesService;

    /** @var String */
    private $date;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->date = Carbon::now()->format('Y-m-d');

        $this->exratesService = new ExchangeratesService();
    }

    /**
     * Тест получения курсов валют из стороннего источника (API)
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @return void
     */
    public function test_get_from_external_source(): void
    {
        $result = $this->exratesService->getFromExternalSource($this->date);
        $this->assertNotNull($result);
    }
}
