<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class Exchangerate
 *
 * @package App\Models
 * @property int $id
 * @property string $code
 * @property string $name
 * @property int $scale
 * @property mixed $official_rate
 * @property string $on_date
 * @property string $on_time
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Exchangerate newModelQuery()
 * @method static Builder|Exchangerate newQuery()
 * @method static Builder|Exchangerate query()
 * @method static Builder|Exchangerate whereCode($value)
 * @method static Builder|Exchangerate whereCreatedAt($value)
 * @method static Builder|Exchangerate whereId($value)
 * @method static Builder|Exchangerate whereName($value)
 * @method static Builder|Exchangerate whereOfficialRate($value)
 * @method static Builder|Exchangerate whereOnDate($value)
 * @method static Builder|Exchangerate whereOnTime($value)
 * @method static Builder|Exchangerate whereScale($value)
 * @method static Builder|Exchangerate whereUpdatedAt($value)
 */
class Exchangerate extends Model
{
    use HasFactory;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'official_rate' => 'decimal:4'
    ];

    /**
     * @var string
     */
    protected $table = 'exchange_rates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'name',
        'scale',
        'official_rate',
        'on_date',
        'on_time'
    ];
}
