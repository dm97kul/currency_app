<?php

namespace Database\Factories;

use App\Models\Exchangerate;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class ExchangerateFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Exchangerate::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'code' => 'USD',
            'name' => 'Доллар США',
            'scale' => 1,
            'official_rate' => 2.64,
            'on_date' => Carbon::now()->format('Y-m-d'),
            'on_time' => Carbon::now()->format('H:i:s')
        ];
    }
}
