<?php

namespace App\Services;

use GuzzleHttp\Client;

/**
 * Class API
 * @package App\Services
 */
class ApiService
{
    /** @var Client  */
    protected $client;

    /**
     * API constructor.
     * @param string $base_uri
     */
    public function __construct(string $base_uri)
    {
        $this->client = new Client(['base_uri' => $base_uri]);
    }

    /**
     * @param string $uri
     * @param array $query
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get(string $uri, array $query = [])
    {
        return $this->client->request('GET', $uri, ['query' => $query]);
    }
}
