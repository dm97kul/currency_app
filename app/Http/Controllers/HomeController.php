<?php

namespace App\Http\Controllers;

use App\Models\Exchangerate;
use App\Services\ExchangeratesService;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $on_date = isset($request['on_date']) ? Carbon::parse($request['on_date'])->format('Y-m-d') : Carbon::now()->format('Y-m-d');

        $data = [
            'curr_date' => Carbon::parse($on_date)->format('Y-m-d'),
            'curr_rates' => (new ExchangeratesService())->getExchangeRatesByDate($on_date)
        ];

        return view('app.views.home', compact('data'));
    }
}
