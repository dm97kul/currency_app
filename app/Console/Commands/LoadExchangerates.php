<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use App\Services\ExchangeratesService;
use Exception;

/**
 * Class LoadExchangerates
 * @package App\Console\Commands
 */
class LoadExchangerates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exchangerates:load';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Загрузка курса валют на текущую дату';

    /** @var ExchangeratesService */
    protected $exchangeratesService;

    /** @var String */
    protected $currentDate;

    /** @var String */
    protected $cacheKey;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws Exception
     */
    public function handle(): void
    {
        $this->info('Команда запущена');
        $this->init();

        try {
            $exratesInDb = $this->exchangeratesService->getFromDatabase($this->currentDate);

            if (count($exratesInDb) === 0)
            {
                $result = $this->exchangeratesService->getFromExternalSource($this->currentDate);

                if (! is_null($result)) {
                    $this->exchangeratesService->setToDatabase($result);
                    $this->exchangeratesService->setToCache($this->cacheKey, $result);
                }
            }
        } catch (Exception $e) {
            throw $e;
        }

        $this->info('Команда завершена');
    }

    /**
     * @return void
     */
    protected function init(): void
    {
        $this->exchangeratesService = new ExchangeratesService();
        $this->currentDate = Carbon::now()->format('Y-m-d');
        $this->cacheKey = 'exchange_rates_' . $this->currentDate;
    }
}
