<?php

namespace Tests\Unit\Services\ExchangeratesService;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Carbon\Carbon;
use App\Services\ExchangeratesService;
use App\Traits\ArrayTestCaseTrait;
use App\Models\Exchangerate;
use Tests\TestCase;

/**
 * Class GetFromDatabaseTest
 * @package Tests\Unit\Services\ExchangeratesService
 */
class GetFromDatabaseTest extends TestCase
{
    use RefreshDatabase, ArrayTestCaseTrait;

    /** @var Exchangerate */
    private $exrates;

    /** @var ExchangeratesService */
    private $exratesService;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->exratesService = new ExchangeratesService();

        $this->exrates = Exchangerate::factory()->count(3)->create()->toArray();
    }

    /**
     * Тест получения курсов валют из базы данных
     *
     * @return void
     */
    public function test_get_from_database(): void
    {
        $result = $this->exratesService->getFromDatabase(Carbon::now()->format('Y-m-d'))->toArray();

        $this->assertArraySimilar($result, $this->exrates);
    }
}
